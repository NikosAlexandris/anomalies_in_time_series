# Anomalies In Time Series

Demonstrating algorithms detecting anomalies in time series (in Python and Jupyter Notebooks):

- Median absolute deviation (MAD)
- Convolutional Smoothing & Decomposition via `tsmoothie`
- Isolation Forest
- Inter-quartile range (IQR)
